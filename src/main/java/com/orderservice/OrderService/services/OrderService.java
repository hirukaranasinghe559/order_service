package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.entities.OrderEntity;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);
    @Autowired
    private OrderRepository repository;

    public List<OrderDTO> getOrdersByUserId(String id){
        List<OrderDTO> orders = null;
        try {
             orders = repository.findOrdersByUserId(id)
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId().toString(),
                            order.getOrder_id(),
                            order.getUser_id()
                    )).collect(Collectors.toList());

        }
        catch (Exception e){
            LOGGER.warn("Exception in GetOrdersByUserId");
        }
        return orders;
    }

    public boolean createOrderByUserId(OrderDTO order){
        try {
            OrderEntity orderEntity = new OrderEntity(
                    Long.parseLong(order.getId()),
                    order.getOrderId(),
                    order.getUserId()
            );
            LOGGER.info("Created Order Entity with " + orderEntity);
            repository.save(orderEntity);
            return true;
        }
        catch (Exception e){
            LOGGER.warn("************* Exception in GetOrdersByUserId *************" + e.getMessage());
        }
        return false;
    }
}
