package com.orderservice.OrderService.controller;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final String id){
        return orderService.getOrdersByUserId(id);
    }

    @PostMapping(value = "create-order", consumes = "application/json")
    public boolean createOrderByUserId(@RequestBody OrderDTO order){
        return orderService.createOrderByUserId(order);
    }
}
