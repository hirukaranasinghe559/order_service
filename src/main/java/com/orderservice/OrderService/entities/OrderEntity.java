package com.orderservice.OrderService.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    private Long id;
    private String order_id;
    private String user_id;

    public OrderEntity(){}

    public OrderEntity(Long id, String orderId, String userId) {
        this.id = id;
        this.order_id = orderId;
        this.user_id = userId;
    }

    public Long getId() {
        return id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getUser_id() {
        return user_id;
    }
}
